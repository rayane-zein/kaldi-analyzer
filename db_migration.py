import sqlite3


def create_db(db_name, table_name):

    con = sqlite3.connect(db_name)
    cur = con.cursor()
    cur.execute('''CREATE TABLE IF NOT EXISTS '%s'('line_id' text, 'ref_words' text, 'hyp_words' text,'op_result' text, 'word_position' integer,'word_origin' text)''' % table_name)

    return


def migrate_data(db_name, table_name, word_list):

    con = sqlite3.connect(db_name)
    cur = con.cursor()
    request = 'INSERT INTO %s VALUES(?,?,?,?,?,?)' % table_name
    cur.executemany(request, word_list)
    con.commit()
    con.close()

    return
