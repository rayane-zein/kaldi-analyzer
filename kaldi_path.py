import os
def path(kaldi_path):

    dir_path = []
    for root, dirs, files in os.walk(kaldi_path):
            for dir in dirs:
                dir_path.append({'dir': dir, 'path': os.path.join(root, dir)})

    return dir_path
