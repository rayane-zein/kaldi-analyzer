# Kaldi analyzer - Rayane Zein

This Kaldi analyzer provide an SQLite database (kaldi.db) which contains as much tables as directories 
containing the per_utt_test, per_utt_val, per_utt_train in a given absolute path of a kaldi_per_utt folder.

Each table contains six columns :

- line_id : the line identifier.
- ref_words : the words of the ref lines.
- hyp_words : the words of the hyp lines.
- op_result : a letter among [C,S,I,D].
- word_position : the position of the word in it related line_id, 0 is the first word of the line.
- word_origin : from which per_utt file the word was parsed among [train, val, test].

#### Usage :
`python3 kaldi-utt-parse.py`

`python3 kaldi-utt-parse.py --help` 

`python3 kaldi-utt-parse.py -r [Yes,No]` : To create a text summary from database

#### Create a report :
`python3 kaldi_utt_report`

#### Running unit tests:
`pipenv run pytest -v tests`


This kaldi analyzer does not require any package installation but requires python 3.8
