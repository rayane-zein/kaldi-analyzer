
from match_and_split import line_parser
from match_and_split import split_line
from match_and_split import regex


def parser(file):

    dict_buffer = {"line_ids": [], "ref_words": [], "hyp_words": [], "op_results": [], "word_index": [], "from_file": []}
    parsed_data_output = []

    with open(file) as rawlog:

        nb_line = 0
        nb_matched_lines = 0

        for line_number, line in enumerate(rawlog):

            nb_line = nb_line + 1
            match = regex(line)

            if match is not None:
                line_id = line_parser('line_id', match)

            if match.group('ref') is not None:

                ref_line = line_parser('ref', match)
                ref_words = split_line(ref_line, 'ref_words')
                line_ids = ([line_id] * len(ref_words))

                if file.name == "per_utt_train":
                    word_origin = (["train"]*len(ref_words))
                elif file.name == "per_utt_val":
                    word_origin = (["val"]*len(ref_words))

                elif file.name == "per_utt_test":
                    word_origin = (["test"]*len(ref_words))

                else:
                    print('File name error')

                for word in ref_words:
                    dict_buffer["word_index"].append(ref_words.index(word))

                dict_buffer["line_ids"] = line_ids
                dict_buffer["ref_words"] = ref_words
                dict_buffer["from_file"] = word_origin

                nb_matched_lines = nb_matched_lines + 1

            elif match.group('hyp') is not None:

                hyp_line = line_parser('hyp', match)
                hyp_words = split_line(hyp_line, 'hyp_words')

                dict_buffer["hyp_words"] = hyp_words

                nb_matched_lines = nb_matched_lines + 1

            elif match.group('op') is not None:

                op_line = line_parser('op', match)
                op_results = split_line(op_line, 'op_results')

                dict_buffer["op_results"] = op_results

                nb_matched_lines = nb_matched_lines + 1

                for i in range(len(dict_buffer['ref_words'])):

                    data_to_migrate = (dict_buffer['line_ids'][i], dict_buffer['ref_words'][i], dict_buffer['hyp_words'][i],
                                       dict_buffer['op_results'][i], dict_buffer['word_index'][i], dict_buffer['from_file'][i])

                    parsed_data_output.append(data_to_migrate)

                dict_buffer = {"line_ids": [], "ref_words": [], "hyp_words": [], "op_results": [], "word_index": [], "from_file": []}

            elif match.group('csid') is not None:

                csid_line = line_parser('csid', match)
                csid_score = split_line(csid_line, 'csid_score')

                int_list_csid = list(map(int, csid_score))
                total_number_of_word = sum(int_list_csid)

                if total_number_of_word != len(op_results) != len(hyp_words):

                    print('\n\n/!\\ **  Problem found during parsing process at line n°:', line_number, ' **  /!\\ \n\n', 'error:', line)

                nb_matched_lines = nb_matched_lines + 1

            else:

                print('\n/!\\  **  Problem found at line n°:', line_number, ' **  /!\\ \n\n', 'error:', line, '\n\n')

        match_rate = int((nb_matched_lines / nb_line) * 100)
        print('\nTotal number of lines', nb_line)
        print('Regular expression matching lines', nb_matched_lines)
        print('Match rate:', match_rate, '%')

    return parsed_data_output
