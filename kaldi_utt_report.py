import sqlite3

def utt_report(db_name):
    con = sqlite3.connect(db_name)
    cur = con.cursor()
    r_table_list = "SELECT name FROM sqlite_master WHERE type='table'"
    table_list = cur.execute(r_table_list).fetchall()

    f = open('Kaldi_Report.txt', 'w')
    f.write('\n\n*** Kaldi per_utt summary *** \n\n')

    for table_name in table_list:

        S = ''' select count(op_result) from %s where op_result = 'S' and word_origin = 'train' ''' % table_name
        D = ''' select count(op_result) from %s where op_result = 'D' and word_origin = 'train' ''' % table_name
        I = ''' select count(op_result) from %s where op_result = 'I' and word_origin = 'train' ''' % table_name
        C = ''' select count(op_result) from %s where op_result = 'C' and word_origin = 'train' ''' % table_name
        mcw = '''select ref_words, count(*)  from %s WHERE ref_words not in ('.',',','-','***',"'") group by ref_words order by count(*) DESC, word_origin limit 20'''% table_name
        mcm = '''select ref_words, hyp_words, op_result, count(*)  from %s WHERE ref_words not in ('.',',','-','***',"'",')','(','+',';') AND op_result in ('S','I','D')group by ref_words order by count(*) DESC limit 20 '''% table_name
        muw = '''select count(DISTINCT ref_words)  from %s  where word_origin != 'train' '''%table_name

        rS = cur.execute(S).fetchone()[0]
        rD = cur.execute(D).fetchone()[0]
        rI = cur.execute(I).fetchone()[0]
        rC = cur.execute(C).fetchone()[0]
        rmcw = cur.execute(mcw).fetchall()
        rmcm = cur.execute(mcm).fetchall()
        rmuw = cur.execute(muw).fetchone()

        wer = round((((rS + rD + rI) / (rS + rD + rC))*100),3)
        f.write(f'\n----- {table_name[0]} :\n\n')
        f.write(f'The WER for {table_name[0]} is : {wer}\n\n')
        f.write(f'{rmuw[0]} words are unknown by the model\n\n')

        f.write(f'The most common words in {table_name[0]} are :\n\n')
        for word in rmcw:
            f.write(f'            -{word[0]} : {word[1]} \n ')

        f.write(f'\nThe most common mistakes in {table_name[0]} are :\n\n')
        for word in rmcm:
            f.write(f'      -   {word[0]} -> {word[1]} : {word[2]} : {word[3]} \n ')

    f.write('\n----------------------------------------------')

#utt_report('kaldi.db')

