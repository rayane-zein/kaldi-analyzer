import pytest
import sqlite3
from file_parse import file_parser
import pathlib 

def file_parser_success_validation():
    per_utt_dir = pathlib.Path(__file__).parent / 'kaldi_per_utt_success'
    data_test = file_parser(per_utt_dir.absolute())
    real_data_size = 36

    if len(data_test[0]) == real_data_size:
        is_valide = True
        return is_valide
    else:
        is_valide = False
        return is_valide


def file_parser_failure_validation():
    per_utt_dir = pathlib.Path(__file__).parent / 'kaldi_per_utt_failure'
    data_test = file_parser(per_utt_dir.absolute())
    real_data_size = 36

    if len(data_test[0]) != real_data_size:
        is_not_valide = True
        return is_not_valide
    else:
        is_not_valide = False
        return is_not_valide


def data_migration_validation():

    per_utt_dir = per_utt_dir = pathlib.Path(__file__).parent / 'kaldi_per_utt_success'
    output_test = file_parser(per_utt_dir.absolute())
    data_test = output_test[0]
    db_name_test = output_test[1]
    table_name_test = output_test[2]

    con = sqlite3.connect(db_name_test)
    cur = con.cursor()
    cur.execute('DROP TABLE %s' % table_name_test)
    output_test = file_parser(per_utt_dir)

    request = '''SELECT count(*) FROM %s ''' % table_name_test
    cur.execute(request)
    r = cur.fetchone()

    exp_nb_row = 36
    db_nb_row = r[0]
    print(r)

    if exp_nb_row == db_nb_row:
        is_valide = True
        cur.execute('DROP TABLE %s' % table_name_test)
        return is_valide

    else:
        is_valide = False
        cur.execute('DROP TABLE %s' % table_name_test)
        return is_valide
    con.close()


def test_data_migration():
    assert data_migration_validation() == True

def test_file_parser_failure():
    assert file_parser_failure_validation() == True, 'One or more words were not parsed'

def test_file_parser_success():
    assert file_parser_success_validation() == True, 'One or more words were not parsed '

