import re


def regex(line):

    test_str = line

    regex = (r"(?P<line_id>.*)ref\w*(?<=ref)\s{2}(?P<ref>.*\w*.*)|"
             r".*(?<=hyp)\s{2}(?P<hyp>.*\w*.*)|"
             r".*(?<=op)(?P<op>.*\w.*)|"
             r".*(?<=csid)(?P<csid>.*\w*)")

    p = re.compile(regex)
    match = p.search(test_str)

    return match


def line_parser(line_name, match):
    if match.group(line_name) is not None:
        match_group_line_name = str(match.group(line_name)).strip()

        return match_group_line_name


def split_line(line, word_list_name):
    import re

    split_pattern = r' +'
    word_list = (re.split(split_pattern, line))

    return word_list
