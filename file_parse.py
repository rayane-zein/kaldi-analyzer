from kaldi_parser import parser
from db_migration import create_db
from db_migration import migrate_data
from kaldi_path import path
import os

def file_parser(kaldi_path):

    data_test = []
    dir_path = path(kaldi_path)

    for i, file in enumerate(dir_path):
        db_name = 'kaldi.db'
        table_name = dir_path[i]['dir']
        create_db(db_name, table_name)

        with os.scandir(dir_path[i]['path']) as it:
            for entry in it:
                print('\n', entry.path)

                data = parser(entry)
                data_test = data
                db_name_test = db_name
                table_name_test = table_name
                print(db_name, table_name)
                migrate_data(db_name, table_name, data)
                
    return data_test, db_name_test, table_name_test




