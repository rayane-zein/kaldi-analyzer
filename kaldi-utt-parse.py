from file_parse import file_parser
from kaldi_utt_report import utt_report
import argparse
import time

parser = argparse.ArgumentParser(prog="kaldi-utt-parse.py",
                                 description="Process the given kaldi per_utt "
                                             "folder path to create a kaldi "
                                             "per_utt database")

parser.add_argument("Path", type=str, help="The path to the kaldi per_utt folder", metavar="Absolute path to the Kaldi per_utt folder")
parser.add_argument("-r", "--report", help="Generate a kaldi per_utt text summary", type=str, metavar="Generate a Kaldi per_utt summary from database", choices=["Yes", "No"])

args = parser.parse_args()

start_time = time.time()
file_parser(args.Path)
#file_parser('/home/zeinr/Bureau/Git/teklia/kaldi_per_utt')

if args.report == 'Yes':
    utt_report('kaldi.db')
    print('\n Report Created succesfully')

print('\n Execeution Time %s s' % (time.time() - start_time))
